# MODULES
module "network" {
  source = "./test-network-module"
}

module "test_vm" {
  source        = "./test-vm-module"
  network_id_test    = module.network.network_id_test
  subnetwork_id_test = module.network.subnetwork_id_test
}