# réseau pour test
resource "google_compute_network" "network_test" {
  name                    = "network-test"
  auto_create_subnetworks = false
}

# sous-réseau pour test
resource "google_compute_subnetwork" "subnetwork_test" {
  name          = "subnetwork-test"
  ip_cidr_range = "10.0.0.0/24"
  region        = "us-east1"
  network       = google_compute_network.network_test.id
}

# firewall ssh
resource "google_compute_firewall" "firewall_ssh_test" {
  name    = "allow-ssh-test"
  network = google_compute_network.network_test.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-test"]
}

# firewall http https
resource "google_compute_firewall" "firewall_http_test" {
  name    = "allow-http-https-test"
  network = google_compute_network.network_test.self_link

  allow {
    protocol = "tcp"
    ports    = ["80", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["server-test"]
}
