output "instance_user" {
  value = var.user
}

output "test_instance_internal_ip" {
  value = module.test_vm.test_instance_internal_ip
}

output "test_instance_external_ip" {
  value = module.test_vm.test_instance_external_ip
}
