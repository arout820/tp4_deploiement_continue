variable "network_id_prod" {
  type = string
}

variable "subnetwork_id_prod" {
  type = string
}

variable "user" {
  default     = "aroutioun.tsakanian"
  description = "Nom de l'utilisateur"
  type        = string
}