# TP4 DEPLOIEMENT CONTINUE

### Création du credentials.json ou clé de compte de service

Dans GCP - https://console.cloud.google.com/ - aller sur :

- 1 - IAM et administration
- 2 - Compte de service
- 3 - Créez un compte de service
- 4 - Entrez un nom puis continuez
- 5 - Séléctionnez le rôle propriétaire (ou éditeur) puis finalisez la création
- 6 - Cliquez sur le compte crée
- 7 - Cliquez sur clé en haut
- 8 - Cliquer sur ajouter une clé, en json
- 9 - Télécharger la clé en le renommant credentials.json
- 10 - Mettez le dans le dossier terraform/private (le dossier private n'existe pas créez le)