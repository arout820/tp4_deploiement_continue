#________________________________________________________________________________________#
# ------ ETAPE 1/2 - CREATION DOSSIER ET DOCKER COMPOSE------ #
echo -e "\033[1;35m- ETAPE 1/2 - CREATION DOSSIER ET DOCKER COMPOSE\033[0m"
mkdir -p gitlab
export GITLAB_HOME=$(pwd)/gitlab 
echo "GITLAB_HOME = $GITLAB_HOME"

cd gitlab
cp ../docker-compose.yml .

docker-compose up -d
sleep 10
docker exec -it gitlab-ce grep 'Password:' /etc/gitlab/initial_root_password

#________________________________________________________________________________________#
# ------ ETAPE 2/2 - TUTO POUR CONFIGURER GITLAB ------ #
echo -e "\033[1;35m- ETAPE 2/2 - TTUTO POUR CONFIGURER GITLAB\033[0m"

echo "Maintenant que les containers ont été crées, il faut configurer le gitlab local, allez donc sur l'url et effectuer les actions plus bas, lisez attentivement:"
echo -e "\e[34;4mhttp://localhost:80\e[0m\n" 

echo -e "- Ensuite dans gitlab-ce il faut décocher "Sign-up enabled" pour la création de nouveaux comptes : \e[34;4mhttp://localhost/admin/application_settings/general#js-signup-settings\e[0m"

echo -e "- Changez le nom d'utilisateur : \e[34;4mhttp://localhost/-/profile/account\e[0m"

echo -e "- Changez le mot de passe : \e[34;4mhttp://localhost/-/profile/password/edit\e[0m"

echo -e "- Puis il faudra vous reconnecter avec les nouveaux idientifiants."

echo -e "- Vous devez également dans les paramètres d'import de votre compte admin **Import and exports settings**, cochez la case pour "**url respository**", cela vous permettra de pouvoir fetch votre projet dans votre container gitlab local : \e[34;4mhttp://localhost/admin/application_settings/general\e[0m"

echo -e "- Maintenant vous pouvez lancer **le deuxième script** qui permettra la création du runner, oubliez pas de le lancer en sudo sinon la partie de configuration du **config.toml** ne fonctionnera pas"