output "network_id_dev" {
  value = google_compute_network.network_dev.id
}

output "subnetwork_id_dev" {
  value = google_compute_subnetwork.subnetwork_dev.id
}