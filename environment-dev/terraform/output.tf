output "instance_user" {
  value = var.user
}

output "dev_instance_internal_ip" {
  value = module.dev_vm.dev_instance_internal_ip
}

output "dev_instance_external_ip" {
  value = module.dev_vm.dev_instance_external_ip
}
