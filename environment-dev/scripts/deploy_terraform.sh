#!/bin/bash

# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/tp4_deploiement_continue"
env_dir="$root_dir/environment-dev"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

# Définition du projet GCP
export GCP_PROJECT="tp4-deploiement-continue"

# Configuration du projet GCP
echo "Configuration du projet GCP : $GCP_PROJECT"
gcloud config set project $GCP_PROJECT

# Vérification de la configuration
echo "Vérification de la configuration du projet GCP :"
gcloud config list

# Message de confirmation
echo "Le projet GCP a été configuré avec succès : $GCP_PROJECT"

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 0/2: CREATION BUCKET SI NON EXISTANT ------ #

# Variables
project_id="tp4-deploiement-continue"
storage_class="STANDARD"
region="us-east1"
bucket_name="tp4-bucket-dev"

# Vérifier si le bucket existe
if gsutil ls -b gs://${bucket_name} ; then
    echo "Le bucket existe déjà : gs://${bucket_name}"
else
    # Créer le bucket s'il n'existe pas
    gsutil mb -p ${project_id} -c ${storage_class} -l ${region} gs://${bucket_name}
    echo "Bucket créé : gs://${bucket_name}"

    # Activer le versioning sur le bucket
    gsutil versioning set on gs://${bucket_name}
    echo "Versioning activé sur le bucket : gs://${bucket_name}"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 1/2: VERIFICATION PRESENCE FICHIERS TERRAFORM ------ #
echo -e "\033[1;35m- Etape 1/2: Vérification présence fichiers terraform\033[0m"

# Vérification de la présence du dossier .terraform et du fichier .terraform.lock.hcl
if [ ! -d "$terraform_dir/.terraform" ] || [ ! -f "$terraform_dir/.terraform.lock.hcl" ]; then
    echo -e "\033[33mDossier .terraform ou fichier .terraform.lock.hcl introuvables. Exécution de 'terraform init'...\033[0m"
    cd $terraform_dir
    terraform init -reconfigure
else
    echo -e "\033[32mDossier .terraform et fichier .terraform.lock.hcl déjà présents. Pas besoin d'exécuter 'terraform init'.\033[0m"
fi

#_____________________________________________________________________________________________________________________________________#
# ------ ETAPE 2/2: TERRAFORM APPLY ------ #
echo -e "\033[1;35m- Etape 2/2: Terraform apply\033[0m"
cd "$terraform_dir"

# Exécuter la commande terraform plan et stocker la sortie dans une variable
plan_output=$(terraform plan)

# Vérifier s'il y a des changements dans la sortie du plan
if echo "$plan_output" | grep -q "No changes"; then
    echo -e "\033[32mAucun changement détecté. Pas besoin de lancer terraform apply.\033[0m"
else
    echo -e "\033[33mDes changements ont été détectés. Terraform apply en cours...\033[0m"
    terraform apply -auto-approve
fi