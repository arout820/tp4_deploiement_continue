# Chemins
root_dir="/builds/$GITLAB_USER_LOGIN/tp4_deploiement_continue"
env_dir="$root_dir/environment-dev"
terraform_dir="$env_dir/terraform"
ansible_dir="$env_dir/ansible"

cd $terraform_dir
dev_ip=$(terraform output dev_instance_external_ip | sed 's/"//g')
user=$(terraform output instance_user | sed 's/"//g')

# Génération de l'inventaire avec les adresses IP
echo "[dev]"
echo $dev_ip ansible_user=$user
